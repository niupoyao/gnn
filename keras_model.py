from keras.models import Model
from keras.layers import Input, convolutional, pooling, concatenate, merge, multiply
from keras.layers.core import Dense, Flatten, Reshape, Activation
from keras import regularizers

inputs = Input( shape = (550, 120, 14))
print(inputs.shape)

x = convolutional.Conv2D(14 , (1,5), strides = (1,1), dilation_rate=(1, 1), activation = 'relu', use_bias = True)(inputs)
x = convolutional.Conv2D(22, (1,3), strides = (1,1), dilation_rate=(1, 1), activation = 'relu', use_bias = True)(x)
local_feature = pooling.MaxPooling2D(pool_size = (1,3))(x)
print(local_feature.shape)

x = convolutional.Conv2D(14 , (1,3), strides = (1,1), dilation_rate=(1, 2), activation = 'relu', use_bias = True)(inputs)
x = convolutional.Conv2D(22, (1,3), strides = (1,1), dilation_rate=(1, 4), activation = 'relu', use_bias = True)(x)
global_feature = pooling.MaxPooling2D( pool_size = (1,3) )(x)
print(global_feature.shape)

trending_feature = concatenate([local_feature, global_feature], axis = 2)
print(trending_feature.shape)

x = convolutional.Conv2D(110, (5,1), strides = (5,1), dilation_rate=(1, 1), activation = 'relu', use_bias = True)(trending_feature)
x = convolutional.Conv2D(200, (12,1), strides = (2,1), dilation_rate=(1, 1), activation = 'relu', use_bias = True)(x)
x = convolutional.Conv2D(300, (12,1), strides = (2,1), dilation_rate=(1, 1), activation = 'relu', use_bias = True)(x)
x = pooling.MaxPooling2D( pool_size = (5,1) )(x)
x = convolutional.Conv2D(500, (4,1), strides = (1,1), dilation_rate=(1, 1), activation = 'relu', use_bias = True)(x)
trending_output = Reshape((74,500,1))(x)
print(trending_output.shape)


## Date information

day_info = Input( shape = (120, 7+12+3) )

x = convolutional.Conv1D(5, (1,), strides = 1, activation = 'relu', kernel_regularizer=regularizers.l1(0.1))(day_info)
x = convolutional.Conv1D(10, (5,), strides = 1, activation = 'relu')(x)
x = convolutional.Conv1D(1, (3,), strides = 1, activation = 'hard_sigmoid')(x)
day_factor_1 = pooling.MaxPooling1D( pool_size = (3,) )(x)
print(day_factor_1.shape)

x = convolutional.Conv1D(5, (1,), strides = 1, activation = 'relu', kernel_regularizer=regularizers.l1(0.1))(day_info)
x = convolutional.Conv1D(10, (3,), strides = 1, dilation_rate = (2,), activation = 'relu')(x)
x = convolutional.Conv1D(1, (3,), strides = 1, dilation_rate = (4,), activation = 'hard_sigmoid')(x)
day_factor_2 = pooling.MaxPooling1D( pool_size = (3,) )(x)
print(day_factor_2.shape)

x = concatenate([day_factor_1, day_factor_2], axis = 1)
x = Reshape((74, 1, 1))(x)
day_factor_12 = convolutional.UpSampling2D(size = (1,500))(x)
print(day_factor_12.shape)

x = convolutional.Conv1D(50, (1,), strides = 1, activation = 'relu', kernel_regularizer=regularizers.l1(0.1) )(day_info)
x = convolutional.Conv1D(20, (5,), strides = 1, activation = 'relu')(x)
day_factor_2 = pooling.MaxPooling1D( pool_size = (4,) )(x)
x = Flatten()(x)
x = Dense(500, activation = 'hard_sigmoid')(x)
x = Reshape((1,500,1))(x)
day_factor_3 = convolutional.UpSampling2D(size = (74,1))(x)
print(day_factor_3.shape)

day_factor = multiply([day_factor_12, day_factor_3]) 
day_factor = Activation('sigmoid')(day_factor)
print(day_factor.shape)

intermidiate_output = multiply([trending_output, day_factor]) 
print(intermidiate_output.shape)