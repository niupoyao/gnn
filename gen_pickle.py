# -*- coding: utf-8 -*-
"""
Created on Thu Apr 20 22:12:25 2017

@author: Siamen
"""

import numpy as np
import os
from six.moves import cPickle as pickle

def gen_pickles(data_folder, bForce = False):
    
    years = os.listdir(data_folder)
    
    num_days = 0;
    
    for oneyear in years:
        num_days += len(os.listdir( os.path.join(data_folder,oneyear) ) )
        
    num_stocks = len(open('stock_IDs').readline().split(' '))
    num_attributes = 14
    
    data_set_rough = np.ndarray(shape = (num_days, num_stocks, num_attributes), dtype=np.float32)  
    data_root = '.'
    pickle_file = os.path.join(data_root,'dataset_raw.pickle')
    
    if not os.path.exists(pickle_file) or bForce :    
        count = 0
        for oneyear in years:
            days = os.listdir( os.path.join(data_folder, oneyear))
            
            for oneday in days:
                file = os.path.join(data_folder,oneyear, oneday)
                data_set_rough[count, :, :] = np.loadtxt(file)
                count = count + 1
        data_set_rough = data_set_rough.transpose(1,0,2)
        try:
            f = open(pickle_file, 'wb')    
            pickle.dump(data_set_rough, f, pickle.HIGHEST_PROTOCOL)
            f.close()
        except Exception as e:
            print('Unable to save data to', pickle_file, ':', e)
            raise
    else:
        with open(pickle_file, 'rb') as f:
            data_set_rough = pickle.load(f)
            
    print('data size: ' , np.shape(data_set_rough))        

    period = 94
    predict_day = 4
    batch_size = 100
    data_set_train = np.ndarray(shape = (batch_size, num_stocks, period, num_attributes), dtype=np.float32);
    data_set_label = np.ndarray(shape = (batch_size, num_stocks), dtype = np.float32)
    
    batch_root = 'mini_batches'
    count = 0
    for i in range(num_days - period - predict_day):
        data_set_train[np.mod(i ,batch_size), :, :, :] = (data_set_rough[:, i : i + period, :])
        data_set_label[np.mod(i ,batch_size), :] = data_set_rough[:, i + period + predict_day - 1, 6]/data_set_rough[:, i + period - 1, 6] - 1
        if np.mod(i+1,batch_size) == 0:
            pickle_file = os.path.join(data_root, batch_root, 'train_batch_{:02d}.pickle'.format(count))
            
            try:
                p = pickle.Pickler( open(pickle_file, 'wb') )
                p.fast = True  
                p.dump( {
                        'data_set_label': data_set_label,
                        'data_set_train': data_set_train,
                        })
                print('train_batch_{:02d}.pickle'.format(count) + ' is done')
            except Exception as e:
                print('Unable to save data to', pickle_file, ':', e)
                raise
                
            count += 1
    data_set_train = data_set_train[0:np.mod(i ,batch_size), :, :, :]
    data_set_label = data_set_label[0:np.mod(i ,batch_size), :]
    last_pickle_file = os.path.join(data_root, batch_root, 'train_batch_{:02d}.pickle'.format(count))
    
    try:
        p = pickle.Pickler( open(last_pickle_file, 'wb') )
        p.fast = True  
        p.dump( {
                'data_set_label': data_set_label,
                'data_set_train': data_set_train,
                })
        print('train_batch_{:02d}.pickle'.format(count) + ' is done')
        print('last batch size:', data_set_train.shape)
    except Exception as e:
        print('Unable to save data to', pickle_file, ':', e)
        raise
   
    return
        
gen_pickles('history_data_quantitative', False)
