import os
import numpy as np
from datetime import datetime, timedelta

start_date = "2008-05-01"
stop_date = "2017-04-11"

start_date = input('earlest day (e.g. 2017-04-11): ')
stop_date  = input('last day (e.g. 2017-04-12): ')


stock_ls = open('stock_IDs', 'r', encoding = 'utf-8').read().split(' ');

Qdata = np.empty([len(stock_ls), 14])
Qdata.fill(np.nan)

complete = False

while date <= stop:
    file_name = '{0}/{1:02d}{2:02d}'.format(date.year, date.month, date.day)
    
    try:
        fhandle = open('history_data/' + file_name + '.txt', 'r', encoding = 'utf-8')
    except:
        date = date + timedelta(days=+1)
        continue

    for line in fhandle:
        line = line.replace(',', '')
        line = line.strip()
        data_str = line.split(' ')
        
        sid = data_str[0]       #證券代號
        
        if sid not in stock_ls:
            continue
        
        pos = stock_ls.index(sid)
        volume = float(data_str[1])     #成交股數
        counts = float(data_str[2])     #成交筆數
        value = float(data_str[3])      #成交金額
        
        try:
            day_open = float(data_str[4])       #開盤價
        except:
            day_open = Qdata[pos][3]
        try:
            day_high = float(data_str[5])       #最高價
        except:
            day_high = Qdata[pos][4]
        try:
            day_low = float(data_str[6])        #最低價
        except:
            day_low = Qdata[pos][5]
        try:
            day_close = float(data_str[7])      #收盤價
        except:
            day_open = Qdata[pos][6]
        
        label = data_str[8]          #漲跌指示
        diff_prev = data_str[9]      #收盤價差
        close_diff = {
            '＋': [float(diff_prev), 0], 
            '－': [float('-' + diff_prev), 0], 
            'x':[0, 1], 
            'o':[0, 0]
        }.get(label)
        
        try:
            last_buy = float(data_str[10])      #最後揭示買價
        except:
            last_buy = Qdata[pos][9]
        try:
            last_buy_v = float(data_str[11])    #最後揭示買量
        except:
            last_buy_v = 0.0
        
        try:
            last_sell = float(data_str[12])     #最後揭示賣價
        except:
            last_sell = Qdata[pos][11]
        try:
            last_sell_v = float(data_str[13])   #最後揭示賣量
        except:
            last_sell_v = 0.0
        
        PEr = float(data_str[14])               #預估本益比
        
        
        Qdata[pos] = [volume, counts, value, day_open, day_high, day_low, day_close, close_diff[0], close_diff[1], last_buy, last_buy_v, last_sell, last_sell_v, PEr]
            
    if not complete and not np.any(np.isnan(Qdata)) :
        complete = True
        print('the data is complete after' + '{}-{}-{}'.format(date.year, date.month, date.day) )
    if complete:
        if not os.path.exists('history_data_quantitative/' + str(date.year)):
            os.makedirs('history_data_quantitative/' + str(date.year))
        np.savetxt('history_data_quantitative/' + file_name, Qdata, fmt = '%.2f')

    fhandle.close()
    date = date + timedelta(days=+1)
