from keras.models import Model
from keras.layers import Input, convolutional, pooling, concatenate, merge, multiply
from keras.layers.core import Dense, Flatten, Reshape, Permute, Activation
from keras.layers.recurrent import LSTM
from keras import regularizers

import h5py

import keras.backend as K

def trend_accr(y_true, y_pred):
    return K.mean( K.cast( y_pred*y_true >= 0, 'float32') )

inputs = Input( shape = (550, 94, 14) )


x = convolutional.Conv2D(14 , (1,3), strides = (1,1), activation = 'relu', use_bias = True)(inputs)
x = convolutional.Conv2D(20 , (1,3), strides = (1,1), activation = 'relu', use_bias = True)(x)
x = pooling.MaxPooling2D(pool_size = (1,3))(x)
x = convolutional.Conv2D(120 , (550,1), strides = (1,1), activation = 'relu', use_bias = True)(x)

x = Reshape((30, 120))(x)

x = LSTM(200, return_sequences = True, dropout=0.0, recurrent_dropout=0.0)(x)
x = LSTM(350, return_sequences = True, dropout=0.0, recurrent_dropout=0.0)(x)
outputs = LSTM(550, return_sequences = False, dropout=0.0, recurrent_dropout=0.0)(x)

StockModel = Model(inputs, outputs)

StockModel.compile(optimizer='rmsprop',
              loss='mean_squared_error',
              metrics=[trend_accr])

model_name = input('\nModel name to save: ')

StockModel.save(model_name + '.h5')