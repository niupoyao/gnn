import requests
import os
from bs4 import BeautifulSoup
from datetime import datetime, timedelta

his_dir = 'history_data'

stop_date  = input('earlest day (e.g. 2017-04-11): ')
start_date = input('last day (e.g. 2017-04-12): ')

date = datetime.strptime(start_date, "%Y-%m-%d")
stop = datetime.strptime(stop_date, "%Y-%m-%d")

while date >= stop:

    if not os.path.exists(str(date.year)):
        os.makedirs(str(date.year))
    
    datestr = '{0}/{1:02d}/{2:02d}'.format(date.year-1911, date.month, date.day)
    
    url = 'http://www.twse.com.tw/ch/trading/exchange/MI_INDEX/MI_INDEX.php'
    payload = {
        'qdate':datestr, 
        'selectType':'ALLBUT0999', 
        }
    
    urlpage = requests.post(url, data=payload)
    html = urlpage.content
    soup = BeautifulSoup(html, 'lxml')
    
    try:
        stocks = soup('table')[1]('tr')[2:]
    except:
        print('no data on ' + datestr)
        date = date + timedelta(days=-1)
        continue
    
    filename = os.path.join(his_dir,str(date.year), '{0:02d}{1:02d}.txt'.format(date.month, date.day))
    with open(filename, 'w', encoding = 'utf-8') as fhandle:
        for stock in stocks:
            tags = stock('td')
            sid = tags[0].contents[0].strip()       #證券代號
            name = tags[1].contents[0].strip()      #證券名稱
            
            volume = tags[2].contents[0].strip()    #成交股數
            counts = tags[3].contents[0].strip()    #成交筆數
            value = tags[4].contents[0].strip()     #成交金額
            
            day_open = tags[5].contents[0].strip()      #開盤價
            day_high = tags[6].contents[0].strip()      #最高價
            day_low = tags[7].contents[0].strip()       #最低價
            day_close = tags[8].contents[0].strip()     #收盤價
            
            diff_prev = tags[10].contents[0].strip()    #收盤價差
            try:
                label = tags[9].contents[0].contents[0].strip()
            except:
                label = tags[9].contents[0].strip()
                
            if label == 'X': 
                label = 'x'
            elif not label: 
                label = 'o'
            
            last_buy = tags[11].contents[0].strip()         #最後揭示買價
            try: 
                last_buy_v = tags[12].contents[0].strip()   #最後揭示買量
            except:
                last_buy_v = '--'
            last_sell = tags[13].contents[0].strip()        #最後揭示賣價
            try:
                last_sell_v = tags[14].contents[0].strip()  #最後揭示賣量
            except:
                last_sell_v = '--'
                
            PEr = tags[15].contents[0].strip()      #預估本益比
            
            dat_str = ' '.join([sid, volume, counts, value, day_open, day_high, day_low, day_close, label, diff_prev, last_buy, last_buy_v, last_sell, last_sell_v, PEr])
            
            fhandle.write(dat_str + '\n')
            
    date = date + timedelta(days=-1)
