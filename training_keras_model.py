from keras.models import Model, load_model
from keras.layers import Input, convolutional, pooling, concatenate, merge, multiply
from keras.layers.core import Dense, Flatten, Reshape, Permute, Activation
from keras.layers.recurrent import LSTM
from keras import regularizers
import keras.backend as K

import h5py

import numpy as np
import os
import sys
from six.moves import cPickle as pickle

directory = input('\nwhere is the pickle files (path): ')
model_name = input('\nwhat is your model name: ')

new_model_name = input('\ngive another model name to save after training (leave it empty to overwrite with same name): ')

def trend_accr(y_true, y_pred):
    return K.mean( K.cast( y_pred*y_true >= 0, 'float32') )

if len(new_model_name.strip()) == 0:
    new_model_name = model_name

StockModel = load_model( model_name + '.h5', custom_objects={'trend_accr': trend_accr} )

def gen_from_pickle_file():
    while 1:
        for onebatch in range(20):
            file = 'train_batch_{:02d}.pickle'.format(onebatch)
            fh = open(directory + '/' + file, 'rb')
            data_dict = pickle.load(fh)
            batch_data = data_dict.get('data_set_train')
            batch_label = data_dict.get('data_set_label')
            yield (batch_data, batch_label)

while True:
    epochs = input('how many epoch(s) for this training? (positive integer only)?')
    if len(epochs.strip()) == 0:
        sys.exit('end training')
    try:
        epochs = int(epochs)
        if epochs > 0:
            break
    except:
        pass

StockModel.fit_generator(gen_from_pickle_file(), steps_per_epoch=20, epochs=epochs)

StockModel.save(new_model_name + '.h5')
print('model saved')x