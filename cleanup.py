from datetime import datetime, timedelta

start_date = "2017-04-11"
stop_date = "2009-01-01"

date = datetime.strptime(start_date, "%Y-%m-%d")
stop = datetime.strptime(stop_date, "%Y-%m-%d")

log_file = open('log', 'w')
stock_ls = []

file_name = '{0}/{1:02d}{2:02d}'.format(date.year, date.month, date.day)
fhandle = open('history_data/' + file_name + '.txt', 'r', encoding = 'utf-8')

for line in fhandle:
    line = line.strip()
    data_str = line.split(' ')
    stock_ls.append(data_str[0])
fhandle.close()

log_file.write(file_name + ' has {} stocks\n'.format(len(stock_ls)))

date = date + timedelta(days=-1)
    

while date >= stop:
    file_name = '{0}/{1:02d}{2:02d}'.format(date.year, date.month, date.day)
    
    try:
        fhandle = open('history_data/' + file_name + '.txt', 'r', encoding = 'utf-8')
    except:
        date = date + timedelta(days=-1)
        continue
    
    ls_new = []
    for line in fhandle:
        line = line.strip()
        data_str = line.split(' ')
        stock_ID = data_str[0]
        if stock_ID in stock_ls:
            ls_new.append(data_str[0])
            stock_ls.remove(data_str[0])
    fhandle.close()
    
    if len(stock_ls) > 0:
        log_file.write(file_name + ' {0} left, {1} stock(s) is missing: '.format(len(ls_new), len(stock_ls)))
        for id in stock_ls:
            log_file.write(id + ' ')
        log_file.write('\n')
    stock_ls = ls_new
    
    date = date + timedelta(days=-1)
log_file.close()
        